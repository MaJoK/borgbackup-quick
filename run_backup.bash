#!/bin/bash

script_path="$(realpath "${BASH_SOURCE[0]}")"
dir="$(dirname "$script_path")"

if ! [ -f "$dir/config.sh" ]; then 
    echo "$dir/config.sh" not found! >&2
    exit 1
fi
. "$dir/config.sh"

if [ "x$BORG_REPO" = "x" ] || [ "x$BORG_PASSPHRASE" = "x" ] || [ "x$SYSTEM_NAME" = "x" ]; then
    echo '$BORG_REPO, $BORG_PASSPHRASE or $SYSTEM_NAME' not in "$dir/config.sh" >&2
    exit 1
fi

# Setting this, so the repo does not need to be given on the commandline:
#export BORG_REPO=ssh://username@example.com:2022/~/backup/main

# See the section "Passphrase notes" for more infos.
#export BORG_PASSPHRASE='XYZl0ngandsecurepa_55_phrasea&&123'

# some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

if [ "${#ALLOWED_SSIDS[@]}" -gt 0 ]; then
    info "Checking SSID..."
    ssid="$(LANG=C nmcli -t -f active,ssid dev wifi | egrep '^yes' | cut -d\: -f2)"
    if [ "x$ssid" == "x" ] || ! printf '%s\n' "${ALLOWED_SSIDS[@]}" | grep --line-regexp --fixed-strings "$ssid" > /dev/null; then
        info "$ssid not in allowlist, quiting"
        exit 0
    fi
fi

excluded_command=()
for path in "${EXCLUDED_PATHS[@]}"; do
    excluded_command+=("--exclude")
    excluded_command+=("$path")
done

info "Starting backup"

# Backup the most important directories into an archive named after
# the machine this script is currently running on:

borg create                         \
    --verbose                       \
    --filter AME                    \
    --list                          \
    --stats                         \
    --show-rc                       \
    --compression lz4               \
    --exclude-caches                \
    --exclude ~/.cache            \
    "${excluded_command[@]}"           \
                                    \
    ::"$SYSTEM_NAME-{now}"            \
    ~/

backup_exit=$?

info "Pruning repository"

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machines' archives also:

borg prune                          \
    --list                          \
    --prefix "$SYSTEM_NAME-"        \
    --show-rc                       \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  6               \
    --keep-yearly   2

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 0 ]; then
    info "Backup and Prune finished successfully"
elif [ ${global_exit} -eq 1 ]; then
    info "Backup and/or Prune finished with warnings"
    notify-send 'Borgbackup-Script' 'Something went wrong'
else
    info "Backup and/or Prune finished with errors"
    notify-send 'Borgbackup-Script' 'Something went wrong'
fi

exit ${global_exit}
