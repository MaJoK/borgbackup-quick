#!/bin/bash

script_path="$(realpath "${BASH_SOURCE[0]}")"
dir="$(dirname "$script_path")"


cd "$dir"
script_path="$dir/run_backup.bash"
cat > borgbackup-of-home.service <<EOF
[Unit]
Description=Backup Home Folder using Borgbackup

[Service]
ExecStart="$script_path"
Nice=19
EOF

echo Created borgbackup-of-home.service
mkdir -p ~/.config/systemd/user/
ln -vs "$dir/borgbackup-of-home.service" ~/.config/systemd/user/
ln -vs "$dir/borgbackup-of-home.timer" ~/.config/systemd/user/

systemctl --user enable --now borgbackup-of-home.timer
